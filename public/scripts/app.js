//Constants
const DEBUG = false;
const HIGHER_GENERATION = 8;
const BASE_POKEDEX_LINK = "https://www.pokemon.com/br/pokedex/";
const CONTRAST_TRANSITION_RATE = 100;
const BLINK_RATE = 700;

const searchedEvolvesTo = [];

const genChkBoxesHtml = [
  document.getElementById("first-gen"),
  document.getElementById("second-gen"),
  document.getElementById("third-gen"),
  document.getElementById("fourth-gen"),
  document.getElementById("fifth-gen"),
  document.getElementById("sixty-gen"),
  document.getElementById("seventh-gen"),
  document.getElementById("eighth-gen")
];
const pokemonImageHtml = document.getElementById("pokemon-image");
const pokemonGuessHtml = document.getElementById("pokemon-guess");
const containerHtml = document.getElementsByClassName("container").item(0);
const pokemonEvolveSearch = document.getElementById("pokemon-chain-search");
const ans2Html = document.getElementById("ans2");

//Variables
let generations = [];
let totalNumberOfPokemon;
let validPokemonIds = [];
let randomPokemonId;
let selectedPokemon;

//Functions
//API Consume
function requestGeneration(id) {
  let values;
  let urlToRequest = `https://pokeapi.co/api/v2/generation/${id}`;
  let request = new XMLHttpRequest();
  request.open('GET', urlToRequest, false);

  request.onload = function () {
    if (request.readyState == 4 && request.status == 200) { //Sucesso
      let ans = JSON.parse(request.responseText);
      values = ans;
    }
  };
  request.onerror = function () {
    console.log("Erro:" + request);
  };

  request.send();
  return values;
}
function requestPokemonInfo(search) {
  let values;
  let urlToRequest = `https://pokeapi.co/api/v2/pokemon/${search}`;
  let request = new XMLHttpRequest();
  request.open('GET', urlToRequest, false);

  request.onload = function () {
    if (request.readyState == 4 && request.status == 200) { //Sucesso
      let ans = JSON.parse(request.responseText);
      values = ans;
    }
  };
  request.onerror = function () {
    console.log("Erro:" + request);
  };

  request.send();
  return values;
}
function requestPokemonSpecies(search) {
  let values;
  let urlToRequest = `https://pokeapi.co/api/v2/pokemon-species/${search}`;
  let request = new XMLHttpRequest();
  request.open('GET', urlToRequest, false);

  request.onload = function () {
    if (request.readyState == 4 && request.status == 200) { //Sucesso
      let ans = JSON.parse(request.responseText);
      values = ans;
    }
  };
  request.onerror = function () {
    console.log("Erro:" + request);
  };

  request.send();
  return values;
}
function requestEvolutionChain(search) {
  let values;
  let pokemonSpecies = requestPokemonSpecies(search);
  if ((typeof search == "number" || search !== "") && pokemonSpecies !== undefined) {
    let urlToRequest = pokemonSpecies.evolution_chain.url;
    let request = new XMLHttpRequest();
    request.open('GET', urlToRequest, false);

    request.onload = function () {
      if (request.readyState == 4 && request.status == 200) { //Sucesso
        let ans = JSON.parse(request.responseText);
        values = ans;
      }
    };
    request.onerror = function () {
      console.log("Erro:" + request);
    };

    request.send();
  }
  return values;
}
//Specific purpose
const toggleLoadingSearch = () => {
  if (containerHtml.getElementsByClassName("loader").length == 0) {
    const loader = document.getElementsByTagName("template")[0];
    const clone = loader.content.cloneNode(true);
    containerHtml.appendChild(clone);
  } else {
    containerHtml.removeChild(document.getElementsByClassName("loader")[0]);
  }

  //const loaderTemplate = document.querySelector("#loaderTemplate");
  //console.log(loaderTemplate);
  //console.log(typeof loaderTemplate);
  //containerHtml.innerHtml = loaderTemplate.innerHtml;
}
function searchEvolutionChain(search) {
  let ansEvolutionChain;
  toggleLoadingSearch();
  let originalEvolutionChain = requestEvolutionChain(search);
  if (originalEvolutionChain !== undefined) {
    ansEvolutionChain = [];
    let pokemonUrl = originalEvolutionChain.chain.species.url;
    ansEvolutionChain.push({
      pokemonName: originalEvolutionChain.chain.species.name,
      pokemonId: pokemonUrl.substring(0, pokemonUrl.length - 1).split('/').pop()
    });

    let evolvesToFlag = false;

    let evolvesTo = originalEvolutionChain.chain.evolves_to;
    let evolvesToPosition = 0;
    let evolvesFromQueue = [];
    let evolvesFromShifted = originalEvolutionChain.chain;
    if (ansEvolutionChain.flat(0).find(e => e.pokemonName === search || e.pokemonId === search)) {
      evolvesToFlag = true;
    }
    while (evolvesTo.length - evolvesToPosition !== 0) {
      if (evolvesToFlag) {
        searchedEvolvesTo.push({
          name: evolvesTo[evolvesToPosition].species.name,
          posterBox: null
        });
      }
      const auxPokemonUrl = evolvesTo[evolvesToPosition].species.url;
      ansEvolutionChain.push({
        pokemonName: evolvesTo[evolvesToPosition].species.name,
        pokemonId: auxPokemonUrl.substring(0, auxPokemonUrl.length - 1).split('/').pop()
      });
      if (evolvesTo[evolvesToPosition].evolves_to !== undefined) { //Pokémon Silcoon
        evolvesFromQueue.push({
          pokemon: evolvesTo[evolvesToPosition],
          levelEvolvesToPosition: evolvesToPosition
        });
      }
      if (evolvesToPosition === evolvesTo.length - 1) {
        if (evolvesFromQueue.length !== 0) {
          evolvesFromShifted = evolvesFromQueue.shift().pokemon;
          evolvesTo = evolvesFromShifted.evolves_to;
          evolvesToPosition = evolvesFromShifted.levelEvolvesToPosition;
          evolvesToFlag = false;
          if (evolvesFromShifted !== undefined) {
            pokemonUrl = evolvesFromShifted.species.url;
            if ((evolvesFromShifted.species.name === search) || search === pokemonUrl.substring(0, pokemonUrl.length - 1).split('/').pop()) {
              evolvesToFlag = true;
            } else {
              evolvesToFlag = false;
            }
          }
          evolvesToPosition = 0;
        } else {
          evolvesFromShifted = null;
          pokemonUrl = evolvesTo[evolvesToPosition].species.url;
          if ((evolvesTo !== undefined && evolvesTo.flat(0).find(p => p.species.name === search)) || search === pokemonUrl.substring(0, pokemonUrl.length - 1).split('/').pop()) {
            evolvesToFlag = true;
          } else {
            evolvesToFlag = false;
          }
          evolvesToPosition++;
        }
      } else {
        pokemonUrl = evolvesFromShifted.species.url;
        if ((evolvesFromShifted.species.name === search) || search === pokemonUrl.substring(0, pokemonUrl.length - 1).split('/').pop()) {
          evolvesToFlag = true;
        } else {
          evolvesToFlag = false;
        }
        evolvesToPosition++;
      }
    }
  }
  return ansEvolutionChain;
}
const buildPokemonChainBox = (pokemon) => {
  const pokemonBox = document.createElement("div");
  const pokemonImage = document.createElement("img");
  const pokemonName = document.createElement("h4");
  const pokedexLink = document.createElement("a");


  const name = pokemon.pokemonName;
  const id = pokemon.pokemonId;
  const pokemonInfo = requestPokemonInfo(id);
  if ((isNaN(parseInt(pokemonEvolveSearch.value)) && name.toLowerCase() == pokemonEvolveSearch.value.replace(" ", "-").toLowerCase()) || (!isNaN(parseInt(pokemonEvolveSearch.value)) && pokemonInfo.id == pokemonEvolveSearch.value)) {
    pokemonBox.className = "poster-box-sel";
  } else {
    pokemonBox.className = "poster-box";
  }
  const evolutionFromSearch = searchedEvolvesTo.find(e => e.name === name.toLowerCase() || name == pokemonInfo.id);
  if (evolutionFromSearch !== undefined) {
    pokemonBox.className = "poster-box-staccato";
    evolutionFromSearch.posterBox = pokemonBox;
  }

  pokemonBox.id = name.charAt(0).toUpperCase() + name.slice(1);
  pokemonImage.src = pokemonInfo.sprites.other["official-artwork"].front_default;
  pokemonImage.className = "poster";
  pokemonName.innerText = `#${pokemonInfo.id}\n${name.charAt(0).toUpperCase()}${name.slice(1)}`;
  pokemonName.className = "poster-title";
  pokedexLink.href = BASE_POKEDEX_LINK + name;
  pokedexLink.target = "_blank";
  pokedexLink.className = "link";

  pokedexLink.appendChild(pokemonBox);
  pokemonBox.appendChild(pokemonImage);
  pokemonBox.appendChild(pokemonName);
  containerHtml.appendChild(pokedexLink);
};
const buildEvolutionChain = (evolutionChainArray) => {
  if (evolutionChainArray !== undefined) {
    evolutionChainArray.forEach(buildPokemonChainBox);
  }
  toggleLoadingSearch();
}
function checkGuess(guess, selectedPokemon) {
  if (guess.toLowerCase() == (selectedPokemon.name).toLowerCase()) {
    return true;
  } else {
    return false;
  }
}
const buildGenerationsArray = () => {
  totalNumberOfPokemon = 0;
  let oldInitId = 0;
  for (let i = 0; i < HIGHER_GENERATION; i++) {
    const generation = requestGeneration(i + 1);
    const initialId = i == 0 ? 1 : oldInitId + generations[i - 1].numberOfPokemonSpecies;
    const finalId = initialId + generation.pokemon_species.length - 1;

    generations.push({
      gen: generation,
      id: generation.id,
      numberOfPokemonSpecies: generation.pokemon_species.length,
      initialPokemonId: initialId,
      finalPokemonId: finalId
    });

    totalNumberOfPokemon += generations[i].numberOfPokemonSpecies;
    oldInitId = initialId;
  }
};
const initializeValidPokemonIds = () => {
  validPokemonIds = Array(totalNumberOfPokemon + 1);
  for (let i = 0; i < genChkBoxesHtml.length; i++) {
    validPokemonIds.fill(genChkBoxesHtml[i].checked, generations[i].initialPokemonId, generations[i].finalPokemonId + 1);
  }
};
const evalValidGeneration = (chkBox) => {
  for (let i = 0; i < genChkBoxesHtml.length; i++) {
    if (chkBox === genChkBoxesHtml[i]) {
      validPokemonIds.fill(chkBox.checked, generations[i].initialPokemonId, generations[i].finalPokemonId + 1);
      break;
    }
  }
};
const aleatorySelectPokemon = () => {
  do {
    randomPokemonId = parseInt(Math.random() * totalNumberOfPokemon) + 1;
    if (DEBUG) {
      console.log(randomPokemonId);
    }
  } while (!validPokemonIds[randomPokemonId])
  return requestPokemonInfo(randomPokemonId);
};
const initOrResetSelectedPokemon = () => {
  pokemonGuessHtml.focus();
  selectedPokemon = aleatorySelectPokemon();
  if (DEBUG) {
    console.log(selectedPokemon.name);
  }
  pokemonImageHtml.setAttribute("src", selectedPokemon.sprites.other["official-artwork"].front_default);
  pokemonImageHtml.style.filter = "contrast(0%)";
  pokemonGuessHtml.value = "";
};
const getCurrentChain = () => {
  return containerHtml.getElementsByTagName("div");
};
const removeCurrentChain = () => {
  const chain = getCurrentChain();
  searchedEvolvesTo.splice(0, searchedEvolvesTo.length);
  for (let i = chain.length - 1; i >= 0; i--) {
    if (chain[i].parentNode.parentNode === containerHtml) {
      containerHtml.removeChild(chain[i].parentNode);
    }
  }
}
const showCorrectGuess = () => {
  let timesExec = 0;
  let contrastToSet = 10;
  const timerInterval = setInterval(() => {
    pokemonImageHtml.style.filter = `contrast(${contrastToSet}%)`;
    contrastToSet += 10;
    if (pokemonImageHtml.style.filter == "contrast(100%)") {
      clearInterval(timerInterval);
    }
  }, CONTRAST_TRANSITION_RATE);
};

//Code
buildGenerationsArray();
initializeValidPokemonIds();

initOrResetSelectedPokemon();

pokemonGuessHtml.onchange = () => {
  if (checkGuess(pokemonGuessHtml.value, selectedPokemon)) {
    showCorrectGuess();
    setTimeout(() => { initOrResetSelectedPokemon(); }, 2000)
  } else {
  }
};

pokemonEvolveSearch.onchange = () => {
  removeCurrentChain();
  const search = pokemonEvolveSearch.value;
  if (isNaN(parseInt(search))) {
    buildEvolutionChain(searchEvolutionChain(search.replace(" ", "-").toLowerCase()));
  } else {
    buildEvolutionChain(searchEvolutionChain(search));
  }
};